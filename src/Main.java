import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

import base.*;
import base.cartas.*;
import base.cartas.magia.*;

public class Main {
	public static void main(String[] args) {
		//instanciando objetos
		//lacaio
	UUID uuid = UUID.randomUUID();
	String myRandom = uuid.toString();
	Carta card1 = new Lacaio("Frodo Bolseiro",2,1,1,1);
	Carta card2 = new Lacaio("Aragorn", 5, 7, 6,6);
	Carta card3 = new Lacaio("Legolas", 8, 4, 6,6);
	Carta card4 = new Lacaio("Druida da Noite", 7, 8, 6,6);
	Carta card5 = new Lacaio("Cientista Maluco", 3, 5, 4,4);
	Carta card6 = new Lacaio("Jacicoê , Marty", 6, 4, 5,5);
	Carta card7 = new Lacaio("Leroy Jekins", 6, 2, 5,5);
	Carta card8 = new Lacaio("Pirata sem tesouro", 4, 5, 4,4);
	Carta card9 = new Lacaio("Dragão bebê", 4, 3, 3,3);
	Carta card  = new Lacaio("Mago Negro", 10, 7, 10,10);
	Carta cardx  = new Lacaio("Logan", 10, 10, 10,10);
	
		//magia
	Carta mag1 = new DanoArea("You shall not pass", 4, 7);
	Carta mag2 = new DanoArea("Telecinese", 3, 2);
	Carta mag8 = new Dano("Remove Bender", 6, 7);
	Carta mag9 = new Dano("Poison", 3, 5);
	Carta mag = new Dano("Snooze", 2, 4);
	
	Carta mag3 = new Buff("Katon", 5, 4, 5);
	Carta mag4 = new Buff("Suiton", 5, 4, 5);
	Carta mag5 = new Buff("Fuuton", 5, 4, 5);
	Carta mag6 = new Buff("Raiton", 5, 4, 5);
	Carta mag7 = new Buff("Doton", 5, 4, 5);
	
	
	//Baralho
	Baralho deck = new Baralho();
	ArrayList<Carta> deck2 = new ArrayList<Carta>();
	LinkedList<Carta> deck3 = new LinkedList<Carta>();
	HashSet<Carta> deck4 = new HashSet<Carta>();
	TreeSet<Carta> deck5 = new TreeSet<Carta>(Comparator.comparing(Carta::getId));
	Collection<Carta> deck6 = Arrays.asList(card1, card2,card3, card4, card5, card6,
			card7, card8, card9, card, cardx); //11
	
	// LinkedList
	//colocando cartas no deck
	for (int i = 0;i < 10001; i++){
		deck3.add(mag9);
	}
	//pega o tempo 1
	long listas = System.nanoTime();
	for (int i = 0;i < 10001; i++){
		deck3.get(i);
	}
	System.out.println("A operação get na LinkedList demorou "+ (System.nanoTime() -listas)/1000000 +"ms\n");
	
	//pega o tempo 2
	listas = System.nanoTime();
	for (int i = 0;i < 10001; i++){
		deck3.contains(cardx);
	}
	
	System.out.println("A operação contains na LinkedList demorou "+ (System.nanoTime() -listas)/1000000 +"ms\n");
	
	//Array
	//colocando cartas no deck
	for(int i = 0 ;i < 10001; i++){
		deck2.add(cardx);
	}
	//pega o tempo 1
	long arrays = System.nanoTime();
	for (int i = 0;i < 10001; i++){
		deck2.get(i);
	}
	System.out.println("A operação get no ArrayList demorou "+ (System.nanoTime() -arrays)/1000000 +"ms\n");
	
	//pega o tempo 2
	arrays = System.nanoTime();
	for (int i = 0;i < 10001; i++){
		deck2.contains(mag9);
	}
	System.out.println("A operação contains no ArrayList demorou "+ (System.nanoTime() -arrays)/1000000 +"ms\n");
		
	
	//Hash
	//colocando cartas no deck
	for(int i = 0 ;i < 10001; i++){
		deck4.add(new Lacaio(myRandom ,i+1,i+1,i+1,i+1));
	}
	//pega o tempo 1
	long hashs = System.nanoTime();
	for (int i = 0;i < 10001; i++){
		deck4.contains(mag9);
	}
	System.out.println("A operação contains no Hash demorou "+ (System.nanoTime() -hashs)/1000000 +"ms\n");
	
	//TreeSet
	//colocando cartas no deck
	for(int i = 0 ;i < 10001; i++){
		deck5.add(new Lacaio("Osas"+i ,i,i,i,i));
	}
	System.out.println(deck5);
	//pega o tempo 1
	long treesets = System.nanoTime();
	for (int i = 0;i < 10001; i++){
		deck5.equals(mag9);
	}
	System.out.println("A operação contains no TreeSets demorou "+ (System.nanoTime() -treesets)/1000000 +"ms\n");
	
	
	//Collection
	System.out.println( deck6.stream()
			.filter(p -> p instanceof Lacaio)
			.map(p->(Lacaio) p)
			.sorted((p1,p2)->p2.getAtaque()-p1.getAtaque())
			.findFirst()
			);
	
	System.out.println( deck6.stream()
			.filter(p -> p instanceof Lacaio)
			.map(p->(Lacaio) p)
			.mapToInt(p->p.getAtaque())
			.sum()
			);
	
	System.out.println( deck6.stream()
			.filter(p -> p instanceof Lacaio)
			.map(p->(Lacaio) p)
			.sorted((p1,p2)->p1.getVidaAtual()-p2.getVidaAtual())
			.collect(Collectors.toList())
			);
	
	}
	

}
