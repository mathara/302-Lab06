package util;

import base.cartas.Lacaio;

public class Util {
	public static final int MAX_CARDS = 100000;
	
	public static void buffar(Lacaio lac,int a){
		if (a > 0){
			lac.setVidaAtual(lac.getVidaAtual() + a);
			lac.setVidaMaxima(lac.getVidaMaxima() + a);
			lac.setAtaque(lac.getAtaque() + a);
			alteraNomeFortalecido(lac);
		}
	}
	
	public static void buffar(Lacaio lac, int a, int v){
		if (a > 0 && v > 0){
			lac.setVidaAtual(lac.getVidaAtual() + v);
			lac.setVidaMaxima(lac.getVidaMaxima() + v);
			lac.setAtaque(lac.getAtaque() + a);
			alteraNomeFortalecido(lac);
		}
	}
	
	public static void alteraNomeFortalecido(Lacaio lac){
		lac.setNome( lac.getNome()+ "Buffed");
	}

}
