package base;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

import util.*;


public class Baralho {
	private ArrayList<Carta> vetorCartas;
	
	public Baralho(){
		vetorCartas = new ArrayList<Carta>();
	}
	
	public void adicionarCarta (Carta card){
		if (vetorCartas.size() <  Util.MAX_CARDS){
			vetorCartas.add(card);
			// não preciso de uma váriavel que contenha o tamanho
		}
	}
	
	public Carta comprarCarta(){
		Carta aux = vetorCartas.get(vetorCartas.size()-1);	//pega o lacaio do topo da pilha
		vetorCartas.remove(vetorCartas.size()-1);					//remove da pilha
		return aux;													//Lacaio do topo da pilha
	}
	
	public void embaralhar(){
		Collections.shuffle(vetorCartas);
		ArrayList<Carta> baux = vetorCartas;
		Collections.reverse(baux);
		System.out.println(baux);
	}
	
	public void imprimirBaralho(){
		System.out.println(vetorCartas);
	}
	
	@Override
	public boolean equals(Object obj){
		return ((this.vetorCartas.equals(((Baralho)obj).vetorCartas))?  true : false);
	}
	
	@Override
	public int hashCode( ){
		int hash = 3;
		return 67 * hash + Objects.hashCode(vetorCartas);
	}
}
